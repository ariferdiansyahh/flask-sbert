from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
from dotenv import load_dotenv
from flask_migrate import Migrate
# Muat variabel lingkungan dari .env
basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root@localhost/test'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
migrate = Migrate(app, db)

# Impor dan daftarkan Blueprint
# Pastikan untuk mengimpor Blueprint setelah db untuk menghindari masalah impor sirkuler
from app.routes.routes import testing_bp
app.register_blueprint(testing_bp, url_prefix='/api')

