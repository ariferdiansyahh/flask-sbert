from flask import request, jsonify, Blueprint
from app.models.models import Question, StudentAnswer, ExamType, User, StudentScore, StudentScoreDetail
from sentence_transformers import SentenceTransformer, util
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import stanza
import string
from app import db  

testing_bp = Blueprint('testing', __name__)

# Hitungan skor berurutan
def hitung_skor_jawaban(jawaban_siswa_arr, kunci_jawaban_arr):
    constanta = 1
    if len(jawaban_siswa_arr) != len(kunci_jawaban_arr):
        constanta = 0.5
    else:
        if all(x == y for x, y in zip(jawaban_siswa_arr, kunci_jawaban_arr)):
            constanta = 1
        else:
            constanta = 0.5
    return constanta

def pembulatan_adjusted_score(adjusted_score):
    # Mendapatkan bagian desimal dari adjusted_score
    decimal_part = adjusted_score - int(adjusted_score)

    # Memeriksa digit desimal untuk menentukan rentang yang sesuai
    if decimal_part <= 0.25:
        rounded_score = int(adjusted_score) + 0.25
    elif decimal_part <= 0.50:
        rounded_score = int(adjusted_score) + 0.50
    elif decimal_part <= 0.75:
        rounded_score = int(adjusted_score) + 0.75
    else:
        rounded_score = int(adjusted_score) + 1.00

    return rounded_score

@testing_bp.route('/')
def hello_world():
    return 'Hello, World! API'

@testing_bp.route('/process-and-score', methods=['POST'])
def process_and_score():
    # Unduh dan inisialisasi pipeline untuk bahasa Indonesia dengan Stanza
    stanza.download('id')
    nlp_id = stanza.Pipeline('id')

    # Dapatkan data dari body request JSON
    data = request.get_json()
    exam_type_id = data.get('exam_type_id')
    user_ids = data.get('user_id')

    # Validasi exam_type_id dan user_ids
    if not exam_type_id or not user_ids:
        return jsonify({"error": "Missing exam_type_id or user_ids"}), 404

    # Cek apakah ExamType ada di database
    exam_type = ExamType.query.get(exam_type_id)
    if not exam_type:
        return jsonify({"error": "ExamType Not Found"}), 404

    results = []  # Untuk menyimpan hasil preprocessing dan similarity score

    for user_id in user_ids:
        # Cek apakah User ada di database
        user = User.query.filter(User.id == user_id, User.role_id == 3).first()
        if not user:
            results.append({"user_id": user_id, "error": "User Not Found"})
            continue

        student_score = StudentScore.query.filter_by(exam_type_id=exam_type_id, user_id=user_id).first()
        if not student_score:
            results.append({"user_id": user_id, "error": "Student Score Not Found"})
            continue


        # Mengambil pertanyaan berdasarkan exam_type_id
        questions = Question.query.filter_by(exam_type_id=exam_type_id).all()

        # Preload student_answers untuk semua questions dalam satu query
        all_student_answers = StudentScoreDetail.query.filter_by(student_score_id=student_score.id).all()
        
        student_answers_map = {answer.question_id: [] for answer in all_student_answers}
        for answer in all_student_answers:
            student_answers_map[answer.question_id].append(answer)

        total_score = 0  # Inisialisasi variabel untuk menyimpan total skor
        user_results = []  # Untuk menyimpan hasil preprocessing dan similarity score per user

        # Membuat objek menghapus stopwords
        stopword_factory = StopWordRemoverFactory()
        stopword_remover = stopword_factory.create_stop_word_remover()

        translator = str.maketrans('', '', string.punctuation) # Menghapus tanda baca
        
        # Inisialisasi model SBERT
        model = SentenceTransformer('all-mpnet-base-v2')

        for question in questions:
            # Preprocessing answer key
            preprocessed_answer_key = stopword_remover.remove(question.answer_key).lower()
            preprocessed_answer_key = preprocessed_answer_key.translate(translator)

            #Process scoring SBERT
            answer_key_embedding = model.encode(preprocessed_answer_key, convert_to_tensor=True)

            question_result = {
                "question_id": question.id,
                "preprocessed_answer_key": preprocessed_answer_key,
                "answers": []
            }

            if question.id in student_answers_map:
                for answer in student_answers_map[question.id]:
                    # Preprocessing student answer
                    preprocessed_student_answer = stopword_remover.remove(answer.answer).lower()
                    preprocessed_student_answer = preprocessed_student_answer.translate(translator)

                    #Process scoring SBERT
                    student_answer_embedding = model.encode(preprocessed_student_answer, convert_to_tensor=True)

                    # Menghitung skor kesamaan menggunakan SBERT
                    cosine_scores = util.dot_score(answer_key_embedding, student_answer_embedding)

                    score = cosine_scores.item()
                    
                    # Analisis sintaksis menggunakan Stanza
                    doc_kunci = nlp_id(question.answer_key)
                    doc_murid = nlp_id(answer.answer)

                    # Menentukan nilai constanta berdasarkan nilai sequent
                    if question.sequent:  # Jika sequent True, jalankan fungsi hitung_skor_jawaban

                        # Menghapus tanda baca dan menyimpan kata-kata dalam array
                        kunci_jawaban_arr = [word.text.lower() for sent in doc_kunci.sentences for word in sent.words if word.text != "," if word.upos != 'PUNCT']
                        jawaban_siswa_arr = [word.text.lower() for sent in doc_murid.sentences for word in sent.words if word.text != "," if word.upos != 'PUNCT']

                        kunci_jawaban_tags = [word.upos for sent in doc_kunci.sentences for word in sent.words]
                        jawaban_siswa_tags = [word.upos for sent in doc_murid.sentences for word in sent.words]

                        #Perhitungan soal berurutan
                        constanta = hitung_skor_jawaban(jawaban_siswa_arr, kunci_jawaban_arr)
                        adjusted_score = 100 * constanta


                        question_result["answers"].append({
                            "doc_kunci" : kunci_jawaban_arr,
                            "doc_murid" : jawaban_siswa_arr,
                            "doc_kunci_tags": kunci_jawaban_tags,
                            "doc_murid_tags": jawaban_siswa_tags
                        })
                    else:  # Jika sequent False, set constanta menjadi 1
                        constanta = 1
                        
                        adjusted_score = score * 100 * constanta  # Skor disesuaikan berdasarkan nilai constanta
                    
                    total_score += adjusted_score

                    question_result["answers"].append({
                        "preprocessed_student_answer": preprocessed_student_answer,
                        "similarity_score": score,
                        "constanta": constanta,
                        "adjusted_score": adjusted_score  # Menampilkan skor yang sudah disesuaikan
                    })

                    # Update atau tambah StudentScoreDetail dengan skor yang sudah disesuaikan
                    student_score_detail = StudentScoreDetail.query.filter_by(student_score_id=student_score.id, question_id=question.id).first()
                    if student_score_detail:
                        student_score_detail.score = adjusted_score
                    else:
                        # Jika tidak ada record yang sesuai, Anda mungkin ingin membuatnya disini
                        pass
                    
                    db.session.add(student_score_detail)
                    db.session.commit() 

            user_results.append(question_result)

        average_score = total_score / len(questions) if len(questions) > 0 else 0

        # Update skor siswa dengan rata-rata skor yang baru dihitung
        if student_score:
            student_score.score = average_score  # Update skor dengan nilai rata-rata yang baru
            db.session.commit()

        # Tambahkan hasil per user ke dalam results
        results.append({"user_id": user_id, "results": user_results})

    return jsonify({"message": "Scores processed and updated successfully", "results": results})
