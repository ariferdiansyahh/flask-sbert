# app/models/models.py
from app import db

class ExamType(db.Model):
    __tablename__ = 'exam_types'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    questions = db.relationship('Question', backref='exam_type', lazy=True)
    
class Question(db.Model):
    __tablename__ = 'questions'
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.Text, nullable=False)
    answer_key = db.Column(db.Text, nullable=False)
    sequent = db.Column(db.Boolean, nullable=False)
    exam_type_id = db.Column(db.Integer, db.ForeignKey('exam_types.id'), nullable=False)
    student_answers = db.relationship('StudentAnswer', backref='question', lazy=True)

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    student_answers = db.relationship('StudentAnswer', backref='user', lazy=True)
    role_id = db.Column(db.Integer)

class StudentAnswer(db.Model):
    __tablename__ = 'student_answers'
    id = db.Column(db.Integer, primary_key=True)
    question_id = db.Column(db.Integer, db.ForeignKey('questions.id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    answer = db.Column(db.Text, nullable=False)

class StudentScore(db.Model):
    __tablename__ = 'student_scores'
    id = db.Column(db.Integer, primary_key=True)
    exam_type_id = db.Column(db.Integer, db.ForeignKey('exam_types.id'), nullable=False)
    score = db.Column(db.Float, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)

class StudentScoreDetail(db.Model):
    __tablename__ = 'student_score_details'
    id = db.Column(db.Integer, primary_key=True)
    student_score_id = db.Column(db.Integer, db.ForeignKey('student_scores.id'), nullable=False)
    question_id = db.Column(db.Integer, db.ForeignKey('questions.id'), nullable=False)
    answer = db.Column(db.Text, nullable=False)
    score = db.Column(db.Float, nullable=False)


