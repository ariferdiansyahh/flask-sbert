# Project Name

## Pendahuluan
Flask API untuk automatic scoring soal menggunakan model SBERT

## Prasyarat
Pastikan Anda telah menginstal dependensi berikut sebelum melanjutkan:
- Python
- pip
- Laravel (untuk migrasi database)

## Instalasi

1. Clone repositori ini:
    ```bash
    git clone https://gitlab.com/ariferdiansyahh/flask-sbert
    cd flask-sbert
    ```

2. Buat virtual environment dan aktifkan:
    ```bash
    python -m venv venv
    source venv/bin/activate  # Untuk pengguna Unix/macOS
    venv\Scripts\activate  # Untuk pengguna Windows
    ```

3. Instal dependensi Python:
    ```bash
    pip install -r requirements.txt
    ```

## Menjalankan Aplikasi

1. Pastikan migrasi database di Laravel sudah dijalankan terlebih dahulu. Jalankan perintah berikut di direktori proyek Laravel Anda:
    ```bash
    php artisan migrate
    ```

2. Jalankan aplikasi Flask:
    ```bash
    flask run
    ```

Aplikasi Flask Anda sekarang berjalan di `http://127.0.0.1:5000/`.

